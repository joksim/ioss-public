<?php
require_once('database.php');

$checkIfExistsQuery = 'select count(id) from shopping_list where list_name=:name and creator=:email';

if (!empty($_POST['submit'])) {
    if (!empty($_POST['listName']) && !empty($_POST['email']) && !empty($_POST['password'])) {
        $listName = $_POST['listName'];
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $existsStatement = $dbo->prepare($checkIfExistsQuery);
        $existsStatement->execute(['email' => $email, 'name' => $listName]);
        $returnedRows = $existsStatement->fetchColumn();
        if (!empty($returnedRows) && is_numeric($returnedRows) && $returnedRows>0) {
            ?>
            <h1>ERROR: You already have a list with that name.</h1>
            <?php
        } else {
            //create the list
            $insertQuery = 'insert into shopping_list (list_name, creator, secret) values (:listName, :creator, :secret)';
            $insertStatement = $dbo->prepare($insertQuery);
            $insertStatement->execute(['listName'=>$listName, 'creator'=>$email, 'secret'=>$password]);


            $shoppingListId = $dbo->lastInsertId();
            $currentFavorites = [];
            if (isset($_COOKIE['favorites'])) {
                $currentFavorites = unserialize($_COOKIE['favorites']);
            }
            array_push($currentFavorites, $shoppingListId);
            $serialized = serialize($currentFavorites);
            $result = setcookie('favorites', $serialized, time()+60*60*24*30);
            $_COOKIE['favorites'] = $serialized;
            header('Location:/index.php');


            ?>
            <h1>List Added</h1>
<?php
        }

    }
} else {
    ?>
<!--    <h1>An unexpected error has happened.</h1>-->
<?php
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>New Shopping List</h1>
<div>
    <form method="post" action="add_shopping_list.php">
        <div>
            <label for="listName">List Name</label>
            <input type="text" required name="listName" id="listName">
        </div>
        <div>
            <label for="email">E-mail</label>
            <input type="email" name="email" required id="email">
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password" required id="password">
        </div>
        <div>
            <input type="submit" name="submit" value="Create">
        </div>
    </form>
</div>
</body>
</html>
