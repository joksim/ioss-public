<?php
require_once('database.php');
require_once ('authenticate.php');

$productsQuery = 'select * from products where shopping_list_id=:id order by is_urgent desc,is_bought asc';
if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $productsStatement = $dbo->prepare($productsQuery);
    $productsStatement->execute(['id' => $id]);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Products</h1>
<div>
    <table border>
        <thead>
        <tr>
            <th>Име</th>
            <th>Додаден</th>
            <th>Количина</th>
            <th>Купен</th>
            <th>Ургентен</th>
            <th>Додај</th>
            <th>Одземи</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($productsStatement as $product) {
            ?>
            <tr>
                <?php
                if ($product['is_urgent'] == 1) {
                    ?>
                    <td>
                        <b><a href="/api.php?purpose=productStatus&id=<?= $product['id'] ?>&shoppingList=<?=$id?>"><?= $product['product_name'] ?></a></b>
                    </td>
                    <?php
                } else if ($product['is_bought'] == 1) {
                    ?>
                    <td>
                        <s><a href="/api.php?purpose=productStatus&id=<?= $product['id'] ?>&shoppingList=<?=$id?>"><?= $product['product_name'] ?></a></s>
                    </td>
                    <?php
                } else {
                    ?>
                    <td>
                        <a href="/api.php?purpose=productStatus&id=<?= $product['id'] ?>&shoppingList=<?=$id?>"><?= $product['product_name'] ?></a>
                    </td>
                    <?php
                }
                ?>

                <td><?= $product['created_at'] ?></td>
                <?php
                if ($product['is_bought'] == 0) {
                    ?>
                    <td><?= $product['quantity'] ?></td>
                    <td><?= $product['is_bought'] == 0 ? 'Не' : 'Да' ?></td>
                    <td><?= $product['is_urgent'] == 0 ? 'Не' : 'Да' ?></td>
                    <td>
                        <form method="post" action="api.php">
                            <input type="hidden" name="purpose" value="changeCount">
                            <input type="hidden" name="shoppingListId" value="<?=$id?>">
                            <input type="hidden" name="productId" value="<?=$product['id']?>">
                            <input type="submit" name="submit" value="+1">
                        </form>
                    </td>
                    <td>
                        <form method="post" action="api.php">
                            <input type="hidden" name="purpose" value="changeCount">
                            <input type="hidden" name="shoppingListId" value="<?=$id?>">
                            <input type="hidden" name="productId" value="<?=$product['id']?>">
                            <input type="submit" name="submit" value="-1">
                        </form>
                    </td>
                    <?php
                }
                ?>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

<div>

    <h2>New product</h2>
    <div>
        <form method="post" action="api.php">
            <input type="hidden" name="purpose" value="addProduct">
            <input type="hidden" name="shoppingListId" value="<?=$_GET['id']?>">
            <div>
                <label for="productName">Product Name</label>
                <input type="text" name="productName" required>
            </div>
            <div>
                <label for="quantity">Quantity</label>
                <input type="number" name="quantity" required>
            </div>
            <div>
                <label for="urgent">Urgent</label>
                <input type="checkbox" name="urgent">
            </div>
            <div>
                <input type="submit" name="submit" value="Add">
            </div>
        </form>
    </div>
</div>
</body>
</html>
