<?php
echo 'api.php';
require_once('database.php');
$purpose = '';
if (!empty($_POST['purpose'])) {
    $purpose = $_POST['purpose'];
    if ($purpose === 'addFavorite' && !empty($_POST['shoppingListId']) && is_numeric($_POST['shoppingListId'])) {
        $shoppingListId = $_POST['shoppingListId'];
        $currentFavorites = [];
        if (isset($_COOKIE['favorites'])) {
            $currentFavorites = unserialize($_COOKIE['favorites']);
        }
        array_push($currentFavorites, $shoppingListId);
        $serialized = serialize($currentFavorites);
        $result = setcookie('favorites', $serialized, time() + 60 * 60 * 24 * 30);
        $_COOKIE['favorites'] = $serialized;
        header('Location:/index.php');
    }

    if ($purpose === 'addProduct' && !empty($_POST['shoppingListId']) && is_numeric($_POST['shoppingListId'])) {
        $shoppingListId = $_POST['shoppingListId'];
        $productName = $_POST['productName'];
        $quantity = $_POST['quantity'];
        $urgent = isset($_POST['urgent']);

        $insertProductQuery = 'insert into products (product_name, shopping_list_id, quantity, is_bought, is_urgent, created_at) 
values(:name, :id, :quantity, false, :urgent, now())';
        $insertProductStatement = $dbo->prepare($insertProductQuery);
        $insertProductStatement->execute(['name' => $productName, 'id' => $shoppingListId, 'quantity' => $quantity, 'urgent' => $urgent]);
        header('Location:/shopping_list.php?id=' . $shoppingListId);
    }

    if ($purpose === 'authenticate' && !empty($_POST['shoppingListId']) && is_numeric($_POST['shoppingListId'])) {
        $shoppingListId = $_POST['shoppingListId'];
        $hash = $_POST['hash'];
        $password = $_POST['password'];
        $authorized = [];
        if (md5($password) == $hash) {
            if (isset($_COOKIE['authorized'])) {
                $authorized = unserialize($_COOKIE['authorized']);
            }
            array_push($authorized, $hash.$shoppingListId);
            $serialized = serialize($authorized);
            setcookie('authorized', $serialized, time() + 60 * 60 * 24 * 30);
            $_COOKIE['authorized'] = $serialized;
            echo 'Match!';
            header('Location:/shopping_list.php?id=' . $shoppingListId);
        } else {
            echo 'No Match!';
            header('Location:/shopping_list.php?id=' . $shoppingListId);
        }
    }

    if ($purpose === 'changeCount' && !empty($_POST['shoppingListId']) && is_numeric($_POST['shoppingListId'])) {
        $shoppingListId = $_POST['shoppingListId'];
        $productId = $_POST['productId'];
        $operation = $_POST['submit'];
        if ($operation === '+1') {
            $incrementQuery = 'update products set quantity=quantity+1 where id=:id';
            $incrementStatement = $dbo->prepare($incrementQuery);
            $incrementStatement->execute(['id' => $productId]);
            header('Location:/shopping_list.php?id=' . $shoppingListId);
        } else if ($operation === '-1') {
            $currentCountQuery = 'select quantity from products where id=:id';
            $currentCountStatement = $dbo->prepare($currentCountQuery);
            $currentCountStatement->execute(['id' => $productId]);
            if ($currentCountStatement->fetchColumn() > 0) {
                $decrementQuery = 'update products set quantity=quantity-1 where id=:id';
                $decrementStatement = $dbo->prepare($decrementQuery);
                $decrementStatement->execute(['id' => $productId]);
            }


            header('Location:/shopping_list.php?id=' . $shoppingListId);
        }
    }

}

if (isset($_GET) && isset($_GET['purpose']) && !empty([$_GET['purpose']])) {
    $purpose = $_GET['purpose'];
    if ($purpose === 'productStatus') {
        echo 'products status';
        if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
            $productId = $_GET['id'];
            $isBoughtStatement = $dbo->prepare('select is_bought from products where id=:id');
            $isBoughtStatement->execute(['id' => $productId]);
            $result = $isBoughtStatement->fetchColumn();
            echo 'Result: ' . $result;
            if ($result === '1') {
                $updateStatement = $dbo->prepare('update products set is_bought=false where id=:id');
                $updateStatement->execute(['id' => $productId]);
            } else {
                $updateStatement = $dbo->prepare('update products set is_bought=true,quantity=1 where id=:id');
                $updateStatement->execute(['id' => $productId]);
            }
            header('Location:/shopping_list.php?id=' . $_GET['shoppingList']);
        }
    }
}
