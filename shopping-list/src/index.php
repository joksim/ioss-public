<?php

require_once ('database.php');

//testing area

//

$shoppingListsQuery = 'select * from shopping_list';

if (empty($_GET['all']) && isset($_COOKIE['favorites'])){
    $favorites = unserialize($_COOKIE['favorites']);
    $numFavorites = count($favorites);
    if ($numFavorites>0){
        $generated = str_repeat('?,', $numFavorites-1).'?';
        $favoriteShoppingListsQuery = "select * from shopping_list where id in ($generated)";
        $allShoppingListsStatement = $dbo->prepare($favoriteShoppingListsQuery);
        $allShoppingListsStatement->execute($favorites);
    }
} else{
$allShoppingListsStatement = $dbo->query($shoppingListsQuery);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Shopping lists</h1>
<h2><a href="/index.php?all=true">Show all shopping lists</a></h2>
<h2><a href="/add_shopping_list.php">New Shopping List</a></h2>
<div>
    <table border>
        <thead>
        <tr>
            <th>List Name</th>
            <th>Add to favorites</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach($allShoppingListsStatement as $shoppingList){
                ?>
                <tr>
                    <td><a href="/shopping_list.php?id=<?=$shoppingList['id']?>"><?=$shoppingList['list_name']?></a></td>
                    <td>
                    <form action="api.php" method="post">
                        <input type="hidden" name="purpose" value="addFavorite">
                        <input type="hidden" name="shoppingListId" value="<?=$shoppingList['id']?>">
                        <input type="submit" name="submit" value="Add favorite">
                    </form>
                    </td>
                </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
</body>
</html>
