<?php
require_once('database.php');

$display=true;
if (isset($_GET) && !empty($_GET['id'])) {
    $shoppingListId = $_GET['id'];
    $hashQuery = 'select secret from shopping_list where id =:id';
    $hashStatement = $dbo->prepare($hashQuery);
    $hashStatement->execute(['id' => $shoppingListId]);
    $hash = $hashStatement->fetchColumn();
    $authorized = [];

    if (isset($_COOKIE['authorized'])) {

        $authorized = unserialize($_COOKIE['authorized']);
        if (count($authorized) > 0) {
            if (array_search($hash.$shoppingListId, $authorized)===false) {
                echo 'not found';
$display=true;
                ?>

                <?php

//                die();
            }else{
               echo 'found';
               $display=false;
//                header('Location:/shopping_list.php?id=' . $shoppingListId);
            }
        }
    }
if ($display===true) {


    ?>
    <h1>Must authorize</h1>
    <div>
        <form method="post" action="api.php">
            <div>
                <input type="hidden" name="purpose" value="authenticate">
                <input type="hidden" name="shoppingListId" value="<?= $shoppingListId ?>">
                <input type="hidden" name="hash" value="<?= $hash ?>">
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
            </div>
            <input type="submit" name="submit" value="Authorize">
        </form>
    </div>
    <?php
    die();
}
}
