<?php

class Student
{
// constructor
    protected ?int $first_name = null;
    static ?string $x = null;

    public function __construct($first_name, $last_name)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        self::$x= "Bla";
    }

    public function __get(string $name): mixed {
       return $this->data[$name];
   }

    public function fullName() : String
    {
        echo "My name is " . $this->first_name . " " . $this->last_name . ".\n";
        return $this->first_name . " " . $this->last_name;
    }

    private function setName(String $fname, String $lname, ?String $mName ) {
        $this->first_name = $fname;
        $this->first_name = $fname;
        $this->first_name = $fname;

    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}

$alex = new Student("Petar", "Petkovski");
$alex->fullName();


class CSStudent extends Student
{
    function sum_numbers($first_number, $second_number)
    {
        $sum = $first_number + $second_number;
        echo $this->first_name . " says that " . $first_number . " + " . $second_number . " is " . $sum;
    }
}

$eric = new CSStudent("Ivan", "Ivanovski");
$eric->fullName();
$eric->sum_numbers(3, 5);