<?php
/**
 * Created by PhpStorm.
 * User: joksim
 * Date: 9.11.18
 * Time: 13:32
 */




include("database.php");


$statement = <<<HERE
select `channels`.`id` as id, 
       `channels`.`name` as `name`,
       `channels`.`description` as `description`,
       SUM(messages.is_unread) as num_unread
from channels left outer join `messages`             
    on `channels`.`id` = `messages`.`channel`
    
group by channels.id
order by num_unread desc;
HERE;


$pdostat = $dbo->prepare($statement);

$pdostat->execute();

while ($row = $pdostat->fetch()){

    echo "<a href='channel.php?channel-id={$row['id']}'>{$row['name']} - {$row['description']} - num_unread: {$row['num_unread']} </a><br/>";

}

//$x = $pdostat->fetchAll();


//echo "<pre>";

//var_dump($x);