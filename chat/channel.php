<?php
/**
 * Created by PhpStorm.
 * User: joksim
 * Date: 16.11.18
 * Time: 12:06
 */

include("database.php");

if(!empty($_GET['channel-id'])){
    $channel_id = $_GET['channel-id'];


    if (!empty($_POST['submit'])){
        $insert_q = <<<INSERT
INSERT INTO `messages`(`message_text`, `sender_username`, `sender_email`, `channel`, `time_sent`, is_unread)
VALUES(:msg, :un, :eml, :cid, NOW(), 1)
INSERT;


        $st = $dbo->prepare($insert_q);
        $st->bindParam(":msg", $_POST['msg'], PDO::PARAM_STR);
        $st->bindParam(":un", $_POST['username'], PDO::PARAM_STR);
        $st->bindParam(":eml", $_COOKIE['email'], PDO::PARAM_STR);
        $st->bindParam(":cid", $channel_id, PDO::PARAM_INT);

        $st->execute();

    }



    $query = <<<HERE
select *
from messages
where `messages`.`channel` = :cid
order by `time_sent` asc;
HERE;


    $stat = $dbo->prepare($query);

    $stat->bindParam(":cid", $channel_id, PDO::PARAM_INT);


    $stat->execute();

    while($row = $stat->fetch()){

?>
        <div>
            <h3>Sender: <?=$row['sender_username']?></h3>
            <?=$row['time_sent']?>
            <p>
                <?=$row['message_text']?>
            </p>
        </div>

<?php
    } //while


?>
    <form action="channel.php?channel-id=<?=$channel_id?>" method="post">
        <textarea name="msg" maxlength="200"></textarea>
        <input name="username" type="text" value="<?= $_COOKIE['username']?>"/>
        <input name="submit" type="submit" value="submit"/>
    </form>


<?php
} // if




